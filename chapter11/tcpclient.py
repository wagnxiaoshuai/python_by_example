import socket
import sys

class TcpClient:
    def __init__(self, serv, port):
        self.addr = (serv, port)
        self.client = socket.socket()
        self.client.connect(self.addr)

    def chat(self):
        while True:
            data = input('(quit to exit)> ') + '\r\n'
            self.client.send(data.encode())
            if data.strip() == 'quit':
                break
            rdata = self.client.recv(1024)
            print(rdata.decode())

        self.client.close()

if __name__ == '__main__':
    serv = sys.argv[1]
    port = int(sys.argv[2])
    tcp_client = TcpClient(serv, port)
    tcp_client.chat()
