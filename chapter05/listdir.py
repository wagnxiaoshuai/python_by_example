import os
import sys

def list_files(path):
    if os.path.isdir(path):          # 判断给定的路径是不是目录
        print(path + ':')
        content = os.listdir(path)   # 返回当前目录的文件列表
        print(content)            # 打印当前目录内容
        for fname in content:      # 取出文件列表中的每个文件名
            fname = os.path.join(path, fname)   # 拼接绝对路径
            list_files(fname)      # 调用自身

if __name__ == '__main__':
    list_files(sys.argv[1])
