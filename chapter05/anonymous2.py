from random import randint

def func1(x):
    return x * 2

if __name__ == '__main__':
    nums = [randint(1, 100) for i in range(10)]  # 生成10个数字的列表
    print(nums)
    result = map(func1, nums)         # 使用正常函数加工列表中的数字
    print(list(result))
    result2 = map(lambda x: x * 2, nums)      # 使用匿名函数
    print(list(result2))
