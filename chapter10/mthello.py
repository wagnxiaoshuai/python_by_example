# import threading
#
# def say_hi():
#     print('Hello')
#
# if __name__ == '__main__':
#     for i in range(3):
#         t = threading.Thread(target=say_hi)
#         t.start()

#############################
import threading

def say_hi(word):
    print('Hello %s' % word)

if __name__ == '__main__':
    for word in ['World', 'China']:
        t = threading.Thread(target=say_hi, args=(word,))
        t.start()
