import subprocess

def ping(host):
    retval = subprocess.run(
        # 因为不需要输出，将输出丢弃即可
        'ping -c1 %s &> /dev/null' % host,
        shell=True
    )
    if retval.returncode == 0:
        print('%s:up' % host)
    else:
        print('%s:down' % host)

if __name__ == '__main__':
    ips = ('192.168.113.%s' % i for i in range(1, 255))
    for ip in ips:
        ping(ip)
